#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2022-2024 Pradyumna Paranjape
#
# This file is part of psprudence.
#
# psprudence is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# psprudence is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with psprudence. If not, see <https://www.gnu.org/licenses/>.
#
"""Command-line EntryPoint."""

import asyncio
import platform
import signal
import sys
from os import environ

from desktop_notifier import DesktopNotifier
from desktop_notifier.common import Icon
from xdgpspconf import ConfDisc

from psprudence import DEBUG, print, project_root
from psprudence.command_line import cli
from psprudence.initialize import init_call
from psprudence.prudence import Prudence, create_alerts

DEFAULT_NOTIFICATION = DesktopNotifier(app_name='PSPrudence',
                                       app_icon=Icon(project_root /
                                                     'data/exclaim.png'))
"""Default Notification."""


async def tick(name: str,
               monitor: Prudence,
               persist: int,
               interval: float = 10) -> int:
    """
    Monitor call.

    Parameters
    -----------
    interval
        Update interval
    disable
        disable alerts
    custom
        custom configuration

    Returns
    --------
    int
        exit code
    """
    # It is bad to use a "while true loop"
    # The following loop runs for almost 70 years if interval is 1 second
    print(f'starting monitor for {name}')
    for _ in range(0x7fffffff):
        alert = monitor()
        print(name, 'enabled' * monitor.enabled, alert, mark='bug')
        if alert:
            await DEFAULT_NOTIFICATION.send(title='Alert',
                                            message=alert,
                                            timeout=persist)
        await asyncio.sleep(interval)
    return 0


def quit_gracefully(*args, **kwargs):
    """Quit gracefully"""
    print("Caught interrupt, quitting safely.", mark='info')
    sys.exit(0)


def main() -> int:
    cliargs = cli()
    if cliargs.get('call', 'monitor') == 'init':
        return init_call(**cliargs)
    if platform.system() == 'Linux' and not environ.get('DISPLAY'):
        print('PSPrudent needs graphical interface.', mark='err')
        return 1
    if 'call' in cliargs:
        del cliargs['call']
    config = ConfDisc('psprudence',
                      __file__).flat_config(custom=cliargs.get('custom'))

    persist: int = config.get('global', {'persist': 5})['persist']
    if not cliargs.get('interval'):
        interval = config.get('global', {'interval': 10.})['interval']

    disable = cliargs.get('disable', '')
    # filter
    peripherals: dict[str, Prudence] = {
        name: alert
        for name, alert in create_alerts(config).items() if name not in disable
    }

    if DEBUG:
        print(config, mark='bug', iterate=True)
        print('disabled:', mark='bug', pref='off', pref_s='>', iterate=True)
        print(disable,
              mark='bug',
              pref='off',
              pref_s='>',
              iterate=True,
              indent=1)
        print(f'interval: {interval}', mark='bug')

    signal.signal(signal.SIGINT, quit_gracefully)
    signal.signal(signal.SIGTERM, quit_gracefully)
    loop = asyncio.get_event_loop()
    for name, mon in peripherals.items():
        loop.create_task(tick(name, mon, persist, cliargs.get('interval',
                                                              10.)))
    loop.run_forever()
    return 0


if __name__ == '__main__':
    main()
