#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2022-2024 Pradyumna Paranjape
#
# This file is part of psprudence.
#
# psprudence is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# psprudence is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with psprudence. If not, see <https://www.gnu.org/licenses/>.
#
"""Prudence sensor."""

import json
from collections.abc import Callable, Mapping
from typing import Any

from psprudence import LOGGER
from psprudence.build_meth import build_func_handle

ProbVal = str | float | int | bool | Mapping
"""Probe value"""

JSONS = str


class Prudence():
    """
    Prudence sensor.

    Parameters
    -----------
    alert
        Alert type string sent to notify

    probe
        Probes the sensor.

        The probe must return a value of the form described in :attr:`probe`

        Callable
            Python callable function
        str
            - multi-line shell code-block
            - Path to file and the function that returns value as output
                - python: ``py: /path/to/pyfile:funcname:arg1:arg2:arg3``
                - shell-script: ``sh: /path/to/shfile:funcname:arg1:arg2:arg3``
    units
        Units to display in notification after current value [default: '']
    enabled
        This alert is enabled [default: ``True``]
    **kwargs
        alert_check :  (Callable, str), optional
            Function to be called to check if alert is to be generated.
            Receives output from :attr:`probe`.
            Returns value to be displayed or ``bool``.
        panic :  (Callable, str), optional
            Function to be called if value is actionable.
            Receives output from :attr:`probe`.
        attempt_reset :  (Callable, str), optional
            Attempt to reset warnings.
            Receives output from :attr:`probe`.

        Following kwargs are required to build the default alert check
            min_warn
                Warnings start after this value
            warn_res : float, default: 1.
                Warnings are recorded after every increment of this value
            reverse : bool, default: False
                Direction of panic is reversed (descending)
    """

    def __init__(self,
                 alert: str,
                 probe: Callable | str,
                 units: str = '',
                 enabled: bool = True,
                 **kwargs):
        self.alert: str = alert
        """Alert type string sent to notify."""

        self.units: str = units
        """Units to display after value."""

        self.enabled: bool = enabled
        """This alert is enabled."""

        self.mem: dict[str, Any] = {
            'min_warn': 0,
            'warn_res': 1.,
            'reverse': False,
        }
        """Hold arbitrary memory."""

        self.mem |= {
            key: val
            for key, val in kwargs.items() if key in self.mem
        }
        self.mem['next_warn'] = self.mem['min_warn']

        self._probe: Callable[..., Any | float | bool | None] = self._wrap(
            probe, 'probe')

        self._alert_check: Callable[..., bool] = self._wrap(
            kwargs.get('alert_check', self._default_alert_check),
            'alert_check')

        self._panic: Callable[..., Any] = self._wrap(
            kwargs.get('panic', self._default_panic), 'panic')

        self._attempt_reset: Callable[..., bool] = self._wrap(
            kwargs.get('attempt_reset', self._default_attempt_reset),
            'attempt_reset')

    @property
    def probe(self) -> Callable[..., Any | float | bool | None]:
        """
        This callable is called to probe current value.

        Alert text is its __str__ form.
        You may replace this property suitably.

        Returns
        --------
            ``None``
                Probe failure.
            ``False``
                Do not send alert.
            ``True``
                Send an alert without value.
            mapping
                A python mapping with a required key 'display' mapped to
                a value whose string form (__str__) will be displayed in the
                notification.  The mapping is then passed as received to
                :meth:`alert_check`.
            JSON
                A JSON string that can be converted to the above mapping.  It
                is passed in the received form (str) to :meth:`alert_check`.
            Any
                Value that can be processed by :meth:`alert_check` to generate
                a display string.  If :meth:`alert_check` returns ``True``, the
                raw value's string form (__str__) will be displayed in the
                notification.
        """
        return self._probe

    @probe.setter
    def probe(self, callback: Callable[..., Any | float | bool | None]):
        self._probe = self._wrap(callback, 'probe')

    @property
    def alert_check(self) -> Callable[..., str | bool]:
        """
        Called to decide whether alert is to be signalled.

        Parameters
        -----------
        self
            This parent object is passed to the
            callable as the first positional argument
        *args
            Value returned by :attr:`probe` is passed to this callable.
            Return value from this callable is ignored.

        Returns
        --------
            String that is displayed in the notification or bool.
            If ``True``, display the input value converted to str (__str__).
            If ``False``, don't display alert notification.
        """
        return self._alert_check

    @alert_check.setter
    def alert_check(self, callback: Callable[..., str | bool]):
        self._alert_check = self._wrap(callback, 'alert_check')

    @property
    def panic(self) -> Callable:
        """
        Function called if value is actionable.

        Parameters
        -----------
        self
            This parent object is passed to the callable as
            the first positional argument.
        *args
            Value returned by :attr:`probe` is passed to this callable.
            Return value from this callable is ignored.

        Returns
        --------
            This is meant only for side-effects, all return values are ignored.
        """
        return self._panic

    @panic.setter
    def panic(self, callback: Callable):
        self._panic = self._wrap(callback, 'panic')

    @property
    def attempt_reset(self) -> Callable:
        """
        Called to decide whether to reset next warning value.

        Parameters
        -----------
        self
            This parent object is passed to the
            callable as the first positional argument
        *args
            Value returned by :attr:`probe` is passed to this callable.
            Return value from this callable is ignored.

        Returns
        --------
            This is meant only for side-effects, all return values are ignored.
         """
        return self._attempt_reset

    @attempt_reset.setter
    def attempt_reset(self, callback: Callable):
        self._attempt_reset = self._wrap(callback, 'attempt_reset')

    def __repr__(self):
        kwargs = [
            f'{key}={getattr(self, key)}'
            for key in ('alert', 'units', 'min_warn', 'warn_res', 'reverse',
                        'enabled', 'probe', 'panic')
        ]
        return (str(self.__class__) + '(' + ', '.join(kwargs) + ')')

    def __call__(self, _probe: Any = None) -> str | None:
        """
        Loop call.

        Probe, check if actionable, panic, return formatted notification.
        """
        if (not self.enabled) and (_probe is None):
            return None
        probe = self.probe() if _probe is None else _probe
        if isinstance(probe, bool) or probe is None:
            return self._message(probe, probe)
        try:
            alert_val = self.alert_check(probe)
        except ValueError as err:
            if any('success' in arg for arg in err.args):
                LOGGER.error(f'{self.alert}:')
                LOGGER.error('Probe shell/os command did not print anything.')
                LOGGER.warning('Disabling.')
                return self._message(None)
        if alert_val is False:
            return self._message(alert_val, probe)
        if alert_val is True:
            alert_val = probe
        if isinstance(alert_val, str):
            # JSON
            try:
                alert_val = json.loads(alert_val)
            except json.JSONDecodeError:
                return self._message(alert_val, probe)
        if isinstance(alert_val, Mapping):
            display = alert_val.get('display')
        elif isinstance(alert_val, (int, float)):
            display = alert_val
        else:
            # bad alert value
            LOGGER.error(f'Bad return from alert_val. {alert_val}')
            return self._message(None)
        if isinstance(display, bool) or display is None:
            return self._message(display, probe)
        try:
            display = float(display)
        except ValueError:
            return self._message(None)
        return self._message(display, probe)

    def _wrap(self, func_ref: Callable | str, util: str) -> Callable:
        """
        Confirm that function is a callable.

        Parameter
        -----------
        func_ref
            Function reference.  If callable, return as received.  If str,
            Wrap around a callable.

        util
            Type of callable
        """
        if callable(func_ref):
            return func_ref
        return build_func_handle(func_ref, ' '.join((self.alert, util)))

    def _message(self,
                 value: float | str | bool | None,
                 probe: Any | float | bool | None = None):
        """
        Construct notification message and take appropriate action.

        Parameters
        -----------
        value
            Construct message based on this value.
        probe
            Response from :meth:`probe`
        """
        if value is None:
            # failure
            self.enabled = False
            return None
        if value is False:
            self.attempt_reset(probe)
            return None
        if value is True:
            self.panic(probe)
            return f'</b>{self.alert}</b>: alert'
        self.panic(probe)
        return f'<b>{self.alert}</b>: {value}{self.units}'

    def _parse_probe_val(
            self, val: int | float | Mapping | str) -> tuple[float, str]:
        """
        Interpret value returned by probe for comparison and display.

        Parameters
        -----------
        val
            Value to interpret

        Returns
        --------
            Float value for comparison and string to display.
        """
        if isinstance(val, str):
            # attempt json (also handles string floats)
            try:
                val = json.loads(val)
            except json.JSONDecodeError as err:
                raise ValueError(f'Bad probe value format: {val}') from err
        if isinstance(val, Mapping):
            compare = val.get('compare')
            display = val.get('display', compare)
            compare = display if compare is None else compare
            return compare, str(display)
        float_val = float(val)
        return (float_val, f'{float_val: 0.2f}')

    def _default_alert_check(self,
                             probe: int | float | Mapping | str) -> str | bool:
        """
        Default fallback for :py:attr:`alert_check`.

        Default val input is float. (floatable strings are passed as float)
        Shall be able to process output from
        :meth:`probe`

        Parameters
        -----------
        probe
            current value or JSON/Mapping with keys 'compare' and 'display'

        Returns
        --------
            If alert should be called
        """
        # compare reversed values in the negative domain.
        compare, display = self._parse_probe_val(probe)
        chase = -1 if self.mem['reverse'] else 1
        threshold = chase * self.mem['next_warn']
        sign_compare = chase * compare

        if sign_compare > threshold:
            self.mem['next_warn'] = self.mem['warn_res'] + chase * max(
                threshold, sign_compare)
            return display
        return False

    def _default_attempt_reset(self, probe: int | float | Mapping | str):
        """
        Default fallback for :py:attr:`attempt_reset`

        Parameters
        -----------
        probe
            current value or JSON/Mapping with keys 'compare' and 'display'
        """
        compare, _ = self._parse_probe_val(probe)
        chase = -1 if self.mem['reverse'] else 1
        if (chase * compare) < (chase * self.mem['min_warn']):
            self.mem['next_warn'] = self.mem['min_warn']

    def _default_panic(self, *args, **kwargs):
        """Don't panic by default."""


def create_alerts(config: dict[str, dict[str, Any]]) -> dict[str, Prudence]:
    """
    Create alerts using :py:class:`Prudence`

    Parameters
    -----------
    config
        dict of kwargs for :py:class:`Prudence`

    Returns
    --------
        Callable prudence sensors
    """
    return {
        name: Prudence(**kwargs)
        for name, kwargs in config.items()
        if (name != 'global' and kwargs.get('enabled', True))
    }
