#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2022 Pradyumna Paranjape
#
# This file is part of psprudence.
#
# psprudence is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# psprudence is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with psprudence. If not, see <https://www.gnu.org/licenses/>.
#
"""
Initialize psprudence for MacOS.
- MacOS requires Notifiers to be signed as is noted by
`desktop-notifier <https://github.com/samschott/desktop-notifier?tab=readme-ov-file#notes-on-macos>`__
"""

import subprocess
from pathlib import Path

from psprudence.initialize.generic import OperatingPlatform


class MacPlatform(OperatingPlatform):

    platform = 'Darwin'

    @property
    def svc_path(self) -> Path:
        return Path.home() / "Library/LaunchAgents/psprudence.plist"

    def is_svc_enabled(self) -> bool:
        sysd_units = subprocess.run(['launchd', 'bslist'],
                                    text=True,
                                    capture_output=True)
        if sysd_units.stdout is None:
            raise ChildProcessError("launchd did not return expected output")
        return "psprudence" in sysd_units.stdout

    def enable_svc(self, revert: bool = False) -> int:
        try:
            return super().enable_svc(revert=revert)
        except NotImplementedError:
            return subprocess.call(
                ['launchd', 'unload' if revert else 'load', self.svc_path])
