#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2022-2024 Pradyumna Paranjape
#
# This file is part of psprudence.
#
# psprudence is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# psprudence is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with psprudence. If not, see <https://www.gnu.org/licenses/>.
#
"""Battery Handles (special case)."""

import subprocess

import psutil
from desktop_notifier import DesktopNotifierSync, Urgency
from desktop_notifier.common import Icon

from psprudence import LOGGER, project_root

BATTERY_NOTIFICATION = DesktopNotifierSync(app_name='PSPrudence',
                                           app_icon=Icon(project_root /
                                                         'data/exclaim.png'))
"""Default Notification."""

BATTERY_NOTIFICATION.send = lambda *args, **kwargs: DesktopNotifierSync.send(
    *args,
    BATTERY_NOTIFICATION,
    title='Battery Too Low',
    urgency=Urgency.Critical,
    **kwargs)


def charge() -> int | bool | None:
    """Probe function for battery."""
    battery = psutil.sensors_battery()
    if battery is None:
        return None
    if not battery.power_plugged:
        return False
    return battery.percent


def panic(suspend_at: str | int | float = '10'):
    """
    Battery Actions.

    Parameters
    ------------
    suspend_at
        minutes remaining when computer should suspend

    Notifies
    ---------
    ``notify`` emergency multiple times and suspends if critical
    """
    battery = psutil.sensors_battery()
    if battery is None:
        return
    if battery.power_plugged:
        return
    time_left = battery.secsleft / 60
    suspend = float(suspend_at)
    LOGGER.info(f'{time_left = }', f'{suspend = }')
    if time_left < 2 * suspend:
        BATTERY_NOTIFICATION.send(message='Suspending Session…',
                                  timeout=suspend)
    if time_left < suspend:
        try:
            subprocess.run(['systemctl', 'suspend'],
                           text=True,
                           capture_output=True,
                           check=True)
        except subprocess.CalledProcessError:
            BATTERY_NOTIFICATION.send(message='Falied to suspend.')
