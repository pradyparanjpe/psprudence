#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2022-2024 Pradyumna Paranjape
#
# This file is part of psprudence.
#
# psprudence is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# psprudence is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with psprudence. If not, see <https://www.gnu.org/licenses/>.
#
"""Peripheral Signal Prudence"""

import logging
import os
from pathlib import Path

from psprint import print

project_root: Path = Path(__file__).parent.resolve()

LOGGER = logging.Logger(__name__)
"""Message logging."""

DEBUG = os.getenv('DEBUG') or False
"""Debugging mode."""

if DEBUG:
    LOGGER.setLevel(logging.DEBUG)
    _CH = logging.StreamHandler()
    _CH.setLevel(logging.DEBUG)
    LOGGER.addHandler(_CH)
