***************
Prerequisites
***************

- Python3
- pip

********
Install
********

pip
====
Preferred method

Install
--------

.. tabs::

    .. group-tab:: pip

       .. code-block:: sh
          :caption: install

          pip install git+https://gitlab.com/pradyparanjpe/psprudence.git


    .. group-tab:: module import

       .. code-block:: sh
          :caption: if ``command not found: pip``

          python3 -m pip install git+https://gitlab.com/pradyparanjpe/psprudence.git


Update
-------

.. tabs:: 

    .. group-tab:: pip

       .. code-block:: sh
          :caption: install

          pip install -U git+https://gitlab.com/pradyparanjpe/psprudence.git


    .. group-tab:: module import

       .. code-block:: sh
          :caption: if ``command not found: pip``

          python3 -m pip install -U git+https://gitlab.com/pradyparanjpe/psprudence.git


Uninstall
----------

.. tabs::

    .. group-tab:: pip

       .. code-block:: sh
          :caption: uninstall

          pip uninstall psprudence


    .. group-tab:: module import

       .. code-block:: sh
          :caption: if ``command not found: pip``

          python3 -m pip uninstall psprudence


Cloned
=======
*(Nightly)*

Requirements
--------------
*(Additional,) Only for this installation method.*

- `git <https://git-scm.com/>`__

Pull
-----
.. code-block:: sh

      git pull https://gitlab.com/pradyparanjpe/psprudence.git && cd psprudence


Install
--------

.. tabs::

    .. group-tab:: pip

       .. code-block:: sh
          :caption: install

          pip install .


    .. group-tab:: module import

       .. code-block:: sh
          :caption: if ``command not found: pip``

          python3 -m pip install .

Initialize
======================

Remember to `initialize <usage.html#initialize>`__ before proceeding.
